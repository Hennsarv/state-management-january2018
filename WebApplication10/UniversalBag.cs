﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Dynamic;
using System.Web.SessionState;

namespace WebApplication10
{
    public class UniversalBag : DynamicObject
    {
        dynamic Prop;
                
        public UniversalBag(object prop)
        {
            Prop = prop;
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            try
            {
                result = Prop[binder.Name];
            }
            catch { result = null; }
                return true;
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            Prop[binder.Name] = value;
            return true;
        }
    }
}
