﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace WebApplication10.Controllers
{
    public class HomeController : Controller
    {
        dynamic TempBag => new UniversalBag(TempData);
        dynamic AppBag => new UniversalBag(System.Web.HttpContext.Current.Application);
        dynamic SessBag => new UniversalBag(System.Web.HttpContext.Current.Session);

        public ActionResult Index(int? id, string nimi, string arvamus)
        {
            Dictionary<String, dynamic> KorralikData = new Dictionary<string, dynamic>(); ;
            dynamic KorralikBag = new UniversalBag(KorralikData);

            KorralikBag.Neli = 4;

            if (!ViewData.Keys.Contains("Esimene"))
                ViewData["Esimene"] = "tere esimene";
            else ViewData["Esimene"] = "Tere esimene teist korda";
            if (!ViewData.Keys.Contains("Teine"))
                ViewData["Teine"] = "tere teine";
            else ViewData["Teine"] = "Tere teine teist korda";

            if (!TempData.Keys.Contains("Esimene"))
                TempData["Esimene"] = "tere esimene";
            else TempData["Esimene"] = "Tere esimene teist korda";
            if (!TempData.Keys.Contains("Teine"))
                TempData["Teine"] = "tere teine";
            else TempData["Teine"] = "Tere teine teist korda";

            TempBag.Salajutt = "Henn kirjutas selle siia";
            ViewBag.TempBag = TempBag;
            ViewBag.Korralik = KorralikBag.Neli;
            ViewBag.Vigane = KorralikBag.Seitse;


            //int s = (int)System.Web.HttpContext.Current.Session["Esimene"];
            //System.Web.HttpContext.Current.Session["Esimene"] = s + 1;

            int s = ++SessBag.Esimene;


            ViewBag.Session = s;


            //int a = (int)System.Web.HttpContext.Current.Application["Esimene"];
            //System.Web.HttpContext.Current.Application["Esimene"] = a + 1;

            int a = ++AppBag.Esimene;
            ViewBag.Application = a;



            //ViewBag.SessionStarted = (DateTime)System.Web.HttpContext.Current.Session["Started"];
            //ViewBag.ApplicationStarted = (DateTime)System.Web.HttpContext.Current.Application["Started"];

            ViewBag.SessionStarted = SessBag.Started;
            ViewBag.ApplicationStarted = AppBag.Started;

            ViewBag.TempTest = TempBag.Puudv ?? "puudub";

            ViewBag.Nimi = nimi ?? "puudub";
            ViewBag.Arvamus = arvamus ?? "arvamus puudub";

            HttpCookie h;

            

            h = Request.Cookies["testnimi"];
            ViewBag.Endine = h?.Values["nimi"] ?? "puudub";


            h = h ?? new HttpCookie("testnimi");



            if (nimi != null)
            {
                h.Values["nimi"] = nimi;
                h.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(h);
            }


            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}